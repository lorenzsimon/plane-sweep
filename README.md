<div align="center">
  <img src="assets/img/logo.png" alt="Logo" height="200">

  ### A cli for calculating intersections of lines in a two-dimensional space
</div>

<br>
<br>
<br>

## About the Project

PlaneSweep is a simple Java cli for finding intersections of lines in two-dimensional space. The application reads the lines from a file and returns the line pairs that intersect, together with the intersection point.

## Getting Started

In order to use this software you have to download the [released](https://gitlab.com/lorenzsimon/plane-sweep/-/releases) jar file and move it to your application directory. 


## Usage

Write the lines you want to check for intersections in a file. The format needs to be like this:

```
G    -4.0    10.0     6.0    10.0
I     4.8     2.0     4.8    11.2
K     8.0     9.0     8.0    11.0
H    -2.0     7.2    12.0     7.2
F     4.0     4.0     4.0     8.0
J    10.0    -2.0    10.0     8.5
L     8.0     6.0    12.0     6.0
M     0.0     3.2    18.0     3.2
X     0.0     0.0     0.0     0.0
```

Please note that the last line must have the name x and the coordinates 0 0 0 0.

Run the application with: `java -jar /path/to/jar < /path/to/lines`

You will then receive the output in this format:

```
M I # (x=4.8, y=3.2)
M J # (x=10.0, y=3.2)
L J # (x=10.0, y=6.0)
H F # (x=4.0, y=7.2)
H I # (x=4.8, y=7.2)
H J # (x=10.0, y=7.2)
G I # (x=4.8, y=10.0)
```


## License

Distributed under the GNU GLP License. See `LICENSE` for more information.
