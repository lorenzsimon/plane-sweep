package plane_sweep.ui;

import plane_sweep.geometry.Intersection;
import plane_sweep.geometry.Plane;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.util.Arrays;
import java.util.List;

/**
 * The Shell class consists of static methods that provide an interface for the
 * interaction between the user and the plane-sweep-program.
 */
public final class Shell {

    /**
     * Suppressed default constructor.
     */
    private Shell() {
    }

    /**
     * Represents the starting point of the program.
     *
     * @param args Optional arguments for the program start.
     * @throws IOException If a I/O error occurs.
     */
    public static void main(String[] args) throws IOException {
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(streamReader);

        readLineInput(stdin);
    }

    /**
     * Reads the entered line parameters and passes them to the handler.
     *
     * @param stdin The Reader object that provides the user input.
     * @throws IOException If a I/O error occurs.
     */
    private static void readLineInput(BufferedReader stdin) throws IOException {
        Plane plane = new Plane();
        boolean endOfInput = false;

        while (!endOfInput) {
            String input = stdin.readLine();

            if (input == null) {
                break;
            }

            // Array containing all words, that were separated by spaces.
            String[] tokens = input.trim().split("\\s+");

            /*
            We store whether the passed line parameters represented a valid line
            in a variable (for readability) and update whether we want to keep
            reading lines (no more input in case of invalid line).
             */
            boolean valid = handleLineInput(tokens, plane);
            endOfInput = !valid;
        }
    }

    /**
     * Checks the provided {@code line-parameters} for validity and handles them
     * properly.
     *
     * @param lineParameters The parameters of a entered line.
     * @param plane          The plane to which the provided line may be added.
     * @return {@code True} if the provided line parameters result in a valid
     * line that got added to the plane or {@code false} if the parameters do
     * not result in a valid line and it was not added to the plane.
     */
    private static boolean handleLineInput(String[] lineParameters,
                                           Plane plane) {

        // A line consists of exactly five parameters.
        if (lineParameters.length == 5) {
            String name = lineParameters[0];
            double[] coordinates;

            try {
                coordinates = getCoordinates(lineParameters);
            } catch (NumberFormatException e) {
                printError("The line coordinates need to be real numbers!");
                return false;
            }

            if (isTerminator(name, coordinates)) {
                printIntersections(plane);
                return false;
            } else if (isOblique(coordinates)) {
                printError("The entered coordinates are illegal!");
                return false;
            } else {
                plane.addLine(name, coordinates[0], coordinates[1],
                        coordinates[2], coordinates[3]);
                return true;
            }
        } else {
            printError("The lines must consist of exactly five parameters!");
            return false;
        }
    }

    /**
     * Detaches the coordinates of the line from the rest of the
     * {@code line-parameters} and parses them into {@code double} values.
     * The parameters that represent the coordinates have to be {@code real}
     * numbers.
     *
     * @param lineParameters The line parameters in textual representation.
     * @return The coordinates of the line as a {@code double} array.
     */
    private static double[] getCoordinates(String[] lineParameters) {
        double[] coordinates = new double[4];

        for (int i = 0; i < 4; i++) {
            coordinates[i] = Double.parseDouble(lineParameters[i + 1]);
        }

        return coordinates;
    }

    /**
     * Checks if the provided {@code coordinates} are from a oblique line.
     * The {@code coordinates} have to be of the format:
     * [startX, startY, endX, endY]
     *
     * @param coordinates The coordinates of a line as a {@code double} array.
     * @return {@code True} if the line is oblique, {@code false} if not.
     */
    private static boolean isOblique(double[] coordinates) {
        return (coordinates[0] != coordinates[2])
                && (coordinates[1] != coordinates[3]);
    }

    /**
     * Checks if a provided line is the terminator line.
     *
     * @param lineName        The name of the line.
     * @param lineCoordinates The coordinates of the line.
     * @return {@code True} if the provided line is the terminator line or
     * {@code false} if not.
     */
    private static boolean isTerminator(String lineName,
                                        double[] lineCoordinates) {
        final String terminatorName = "X";
        final double[] terminatorCoordinates = {0.0, 0.0, 0.0, 0.0};

        return terminatorName.equals(lineName)
                && Arrays.equals(terminatorCoordinates, lineCoordinates);
    }

    /**
     * Outputs the intersections of a provided {@code plane} to the screen.
     *
     * @param plane The plane object whose intersections are outputted.
     */
    private static void printIntersections(Plane plane) {
        List<Intersection> intersections = plane.getIntersections();

        for (Intersection i : intersections) {
            System.out.println(i);
        }
    }

    /**
     * Prints an error {@code massage} to the screen.
     *
     * @param message The specifics of the occurred error.
     */
    private static void printError(String message) {
        System.out.println("Error! " + message);
    }
}
