package plane_sweep.geometry;

import searchtree.SearchTree;
import searchtree.SplayTree;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * The plane class is responsible for managing multiple lines in a plane and
 * calculating possible intersections between those lines.
 */
public class Plane {

    /**
     * Represents all lines that are currently in the plane.
     */
    private Set<Line> lines;

    /**
     * Default constructor for a new empty plane.
     */
    public Plane() {
        lines = new HashSet<>();
    }

    /**
     * Adds a new line with the provided line parameters to the plane.
     *
     * @param name   The name of the line to add.
     * @param startX The x coordinate of the starting point of the line to add.
     * @param startY The y coordinate of the starting point of the line to add.
     * @param endX   The x coordinate of the endpoint of the line to add.
     * @param endY   The y coordinate of the endpoint of the line to add.
     */
    public void addLine(String name, double startX, double startY, double endX,
                        double endY) {
        Line newLine = new Line(name, startX, startY, endX, endY);
        lines.add(newLine);
    }

    /**
     * Gets the intersections between the current lines in the plane.
     * <p>
     * This method can only be called if there are no oblique lines, no
     * different lines with identical y coordinates and no overlapping vertical
     * lines in the set of lines of the plane. The caller needs to ensure that.
     * <p>
     * The method simulates a line that sweeps the plane from bottom to top.
     *
     * @return A {@link List} of {@link Intersection intersections} between the
     * current lines in the plane in the order: from bottom to top and in case
     * of identical y coordinates from left to right.
     */
    public List<Intersection> getIntersections() {

        // The events of the sweep line.
        Map<Double, Line> events = getEvents();

        // The vertical lines that currently intersect with the sweep line.
        SearchTree<Double, Line> activeVerticals = new SplayTree<>();

        // The found intersections.
        List<Intersection> intersections = new LinkedList<>();

        for (double breakPoint : events.keySet()) {
            Line temp = events.get(breakPoint);

            switch (temp.getType()) {
            case HORIZONTAL:
                addIntersections(temp, activeVerticals, intersections);
                break;
            case VERTICAL:
                updateActiveVerticals(temp, activeVerticals, breakPoint);
                break;
            case OBLIQUE:
                throw new IllegalStateException("Oblique lines in the Plane.");
            default:
                throw new IllegalStateException("Unknown type of line in the "
                        + "Plane: " + temp.getType());
            }
        }

        return intersections;
    }

    /**
     * Gets the textual representation of the plane.
     *
     * @return A {@link String} that represents the current state of the plane.
     */
    @Override
    public String toString() {
        return lines.toString();
    }

    /**
     * Gets all break points of the sweep line and the associated intersecting
     * line (events).
     * <p>
     * Those brake points are represented by the y coordinates of the
     * {@link #lines} in the plane.
     * <p>
     * Different lines in the plane are not allowed to have identical y
     * coordinates. The caller needs to ensure that.
     *
     * @return A {@link Map} of key-value pairs where the key is the y
     * coordinate and the value the associated line.
     */
    private Map<Double, Line> getEvents() {
        Map<Double, Line> events = new TreeMap<>();

        for (Line l : lines) {

            // There can only be one associated line per break point.
            if (events.containsKey(l.getStartY())
                    || events.containsKey(l.getEndY())) {
                throw new IllegalStateException("Plane has lines with identical"
                        + " y-coordinates.");
            }

            if (l.getType() == Line.LineType.HORIZONTAL) {
                events.put(l.getStartY(), l);
            } else {

                // Lines that are not horizontal cause two events.
                events.put(l.getStartY(), l);
                events.put(l.getEndY(), l);
            }
        }

        return events;
    }

    /**
     * Gets all intersections between a provided horizontal line
     * ({@code horizontal}) and the provided vertical lines ({@code verticals})
     * and adds them to the provided {@link List} of already found intersections
     * ({@code intersections}).
     *
     * @param horizontal    The horizontal line.
     * @param verticals     The vertical lines in a {@link SearchTree}.
     * @param intersections The already found intersections in a {@link List}.
     */
    private void addIntersections(Line horizontal,
                                  SearchTree<Double, Line> verticals,
                                  List<Intersection> intersections) {

        // Lines can go from right to left, so we need to interval of the line.
        double xIntervalStart = min(horizontal.getStartX(),
                horizontal.getEndX());
        double xIntervalEnd = max(horizontal.getStartX(), horizontal.getEndX());

        /*
        For each vertical line in "verticals" whose key (x coordinate) lie
        within the x interval of the horizontal line "horizontal".
         */
        for (Line l : verticals.get(xIntervalStart, xIntervalEnd)) {
            Intersection intersection = new Intersection(horizontal.getName(),
                    l.getName(), l.getStartX(), horizontal.getStartY());

            intersections.add(intersection);
        }
    }

    /**
     * Updates the vertical lines that are "currently" intersecting with the
     * sweep line.
     * <p>
     * The vertical line {@code vertical} must not overlap with one of the lines
     * in {@code verticals}.
     *
     * @param vertical   The vertical line that either gets added or removed
     *                   from the lines that intersect with the sweep line.
     * @param verticals  The vertical lines that are currently intersecting with
     *                   the sweep line.
     * @param breakPoint The current break point of the sweep line.
     */
    private void updateActiveVerticals(Line vertical,
                                       SearchTree<Double, Line> verticals,
                                       double breakPoint) {

        // We need the minimum, because lines can go from top to bottom.
        double yIntervalStart = min(vertical.getStartY(), vertical.getEndY());

        /*
        If the sweep line stops currently at the bottom of the vertical line, we
        add the line to the lines that intersect with the sweep line, else the
        sweep line stopped at the top of the vertical line and the line is not
        intersecting with the sweep line anymore and we remove it.
         */
        if (breakPoint == yIntervalStart) {
            if (verticals.containsKey(vertical.getStartX())) {
                throw new IllegalStateException("Plane has overlapping vertical"
                        + " lines.");
            }

            verticals.add(vertical.getStartX(), vertical);
        } else {
            verticals.remove(vertical.getStartX());
        }
    }
}
