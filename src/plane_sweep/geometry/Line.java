package plane_sweep.geometry;

/**
 * The line class represents two dimensional lines with a name, a start point
 * and an end point.
 */
public class Line {

    /**
     * The name of the line.
     */
    private String name;

    /**
     * The type of the line.
     */
    private LineType type;

    /**
     * The x coordinate of the start point.
     */
    private double startX;

    /**
     * The y coordinate of the start point.
     */
    private double startY;

    /**
     * The x coordinate of the end point.
     */
    private double endX;

    /**
     * The y coordinate of the end point.
     */
    private double endY;

    /**
     * Represents the possible types of a line.
     */
    public enum LineType {

        /**
         * Represents a vertical line.
         */
        VERTICAL,

        /**
         * Represents a horizontal line.
         */
        HORIZONTAL,

        /**
         * Represents a oblique line.
         */
        OBLIQUE
    }

    /**
     * Constructor for a new line with the provided line parameters.
     *
     * @param name   The name of the new line.
     * @param startX The x coordinate of the start point of the new line.
     * @param startY The y coordinate of the start point of the new line.
     * @param endX   The x coordinate of the end point of the new line.
     * @param endY   The y coordinate of the end point of the new line.
     */
    public Line(String name, double startX, double startY, double endX,
                double endY) {
        this.name = name;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;

        if (startY == endY) {
            type = LineType.HORIZONTAL;
        } else if (startX == endX) {
            type = LineType.VERTICAL;
        } else {
            type = LineType.OBLIQUE;
        }
    }

    /**
     * Gets the name of the line.
     *
     * @return The name of the line.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the x coordinate of the start point of the line.
     *
     * @return The x coordinate of the start point of the line.
     */
    public double getStartX() {
        return startX;
    }

    /**
     * Gets the y coordinate of the start point of the line.
     *
     * @return The y coordinate of the start point of the line.
     */
    public double getStartY() {
        return startY;
    }

    /**
     * Gets the x coordinate of the end point of the line.
     *
     * @return The x coordinate of the end point of the line.
     */
    public double getEndX() {
        return endX;
    }

    /**
     * Gets the y coordinate of the end point of the line.
     *
     * @return The y coordinate of the end point of the line.
     */
    public double getEndY() {
        return endY;
    }

    /**
     * Gets the type of the line.
     *
     * @return The type of the line.
     */
    public LineType getType() {
        return type;
    }

    /**
     * Gets a textual representation of the line.
     *
     * @return A {@link String} that represents the line.
     */
    @Override
    public String toString() {
        return name + ": start(" + startX + ", " + startY + "), end(" + endX
                + ", " + endY + ")";
    }
}
