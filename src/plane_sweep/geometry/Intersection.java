package plane_sweep.geometry;

/**
 * An intersection consists of the two names of the intersecting objects and the
 * coordinates of the intersection.
 * <p>
 * This class represents such a intersection between two objects in a two
 * dimensional plane.
 */
public class Intersection {

    /**
     * The name of the intersecting object a.
     */
    private String nameObjectA;

    /**
     * The name of the intersecting object b.
     */
    private String nameObjectB;

    /**
     * The x coordinate of the intersection point.
     */
    private double xCoordinate;

    /**
     * The y coordinate of the intersection point.
     */
    private double yCoordinate;

    /**
     * A Constructor for a new intersection with the provided parameters.
     *
     * @param nameObjectA The name of the object a.
     * @param nameObjectB The name of the object b.
     * @param xCoordinate The x coordinate of the intersection point.
     * @param yCoordinate The y coordinate of the intersection point.
     */
    public Intersection(String nameObjectA, String nameObjectB,
                        double xCoordinate, double yCoordinate) {
        this.nameObjectA = nameObjectA;
        this.nameObjectB = nameObjectB;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    /**
     * Gets a textual representation of the intersection.
     *
     * @return A {@link String} that represents the intersection.
     */
    @Override
    public String toString() {
        return nameObjectA + " " + nameObjectB + " # (x=" + xCoordinate
                + ", y=" + yCoordinate + ")";
    }
}
