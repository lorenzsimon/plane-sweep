package searchtree;

import java.util.List;

/**
 * A Search tree is a data structure, that stores data value of type {@code <V>}
 * associated with a identifying key of type {@code <K>}.
 * <p>
 * Due to the arrangement of the elements (key-value pairs) in the tree, the
 * data structure is sorted by keys.
 *
 * @param <K> The comparable type parameter for the key objects.
 * @param <V> The type parameter for the value objects.
 */
public interface SearchTree<K extends Comparable<K>, V> {

    /**
     * Adds a new key-value pair to the tree.
     *
     * @param key   The key, that identifies the new {@code value}.
     * @param value The value to add to the tree.
     */
    void add(K key, V value);

    /**
     * Removes a key-value pair identified by the provided {@code key} from the
     * tree if an element with the provided {@code key} exists in the tree.
     *
     * @param key The key that identifies the element to be deleted from the
     *            tree.
     */
    void remove(K key);

    /**
     * Gets the value of an element in the tree that is identified by the
     * provided {@code key} if it exists.
     *
     * @param key The key that identifies the searched value.
     * @return The value associated with the provided {@code key}.
     */
    V get(K key);

    /**
     * Gets a list of all values whose keys lie within a given interval
     * [{@code min}, {@code max}].
     * <p>
     * If the end of the interval is less than the start of the interval, the
     * interval is empty.
     *
     * @param min The start of the interval.
     * @param max The end of the interval.
     * @return A list containing all values whose keys lie within the provided
     * interval.
     */
    List<V> get(K min, K max);

    /**
     * Checks if an element identified by the provided {@code key} exists in the
     * tree.
     *
     * @param key The key that identifies the searched element.
     * @return {@code True} if the searched element is in the tree,
     * {@code false} if not.
     */
    boolean containsKey(K key);

    /**
     * Gets a textual representation of the tree.
     *
     * @return The {@link String} that represents the current tree.
     */
    @Override
    String toString();
}
