package searchtree;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * A splay tree is a search tree and implements the {@link SearchTree}
 * interface.
 * <p>
 * In order to guarantee a good runtime of the search tree operations, the tree
 * is balanced in most operations by so called splaying.
 *
 * @param <K> The comparable type parameter for the key objects.
 * @param <V> The type parameter for the value objects.
 */
public class SplayTree<K extends Comparable<K>, V> implements SearchTree<K, V> {

    /**
     * Represents the root of the tree.
     */
    private Node root;

    /**
     * A tree data structure consists of nodes that store the key-value pairs
     * and pointers to (two) other nodes of a tree.
     * This node class represents these node objects.
     * <p>
     * The node class is private and final because the {@link SplayTree} class
     * is the only class that should construct and use nodes.
     */
    private final class Node {

        /**
         * The key object that identifies a key-value pair (and a node) in the
         * tree.
         */
        private K key;

        /**
         * The value object that stores the value of a key-value pair.
         */
        private V value;

        /**
         * A node that has a key smaller than this {@link #key}.
         * Such a node is called a left child of a node.
         */
        private Node leftChild;

        /**
         * A node that has a key bigger than this {@link #key}.
         * Such a node is called a right child of a node.
         */
        private Node rightChild;

        /**
         * Constructor that assigns {@code key} and {@code value} to a new node.
         * <p>
         * A valid node always has a {@code key} and a {@code value}.
         *
         * @param key   The key that identifies the key-value pair of the
         *              constructed node.
         * @param value The value associated with the {@code key}.
         */
        private Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        /**
         * Gets the textual representation of the node.
         *
         * @return The {@link String} that represents the node.
         */
        @Override
        public String toString() {
            return "[Root: " + key + " Left_Child: " + leftChild
                    + " Right_Child: " + rightChild + "]";
        }
    }

    /**
     * Default constructor for a new empty tree.
     */
    public SplayTree() {
        root = null;
    }

    /**
     * Constructor for a new tree with the provided key-value pair in it.
     *
     * @param key   The key of the root node.
     * @param value The value of the root node.
     */
    public SplayTree(K key, V value) {
        root = new Node(key, value);
    }

    /**
     * Adds a new key-value pair to the tree.
     * <p>
     * If there is already a key-value pair with the provided {@code key} in the
     * tree, the provided {@code value} does not get added to the tree and the
     * tree does not get changed.
     * <p>
     * After inserting the new key-value pair, the tree is splayed at this node.
     *
     * @param key   The key, that identifies the new {@code value}.
     * @param value The value to add to the tree.
     */
    @Override
    public void add(K key, V value) {
        Node newSubTreeRoot = new Node(key, value);

        if (root == null) {
            root = newSubTreeRoot;
        } else {
            recursiveAdd(root, newSubTreeRoot);
        }
    }

    /**
     * Removes a key-value pair identified by the provided {@code key} from the
     * tree if an element with the provided {@code key} exists in the tree.
     * <p>
     * After the deletion of the node identified by the {@code key}, the tree is
     * splayed at the parent node.
     *
     * @param key The key that identifies the element to be deleted from the
     */
    @Override
    public void remove(K key) {
        LinkedList<Node> searchPath = getSearchPath(key);
        Node target = searchPath.poll();
        Node parent = searchPath.poll();

        if (target == null) {

            // No node was found, so the method gets canceled.
            return;
        } else if (target.rightChild == null && target.leftChild == null) {
            noChildDelete(target, parent);
        } else if (target.leftChild != null && target.rightChild == null) {
            leftChildDelete(target, parent);
        } else if (target.rightChild != null && target.leftChild == null) {
            rightChildDelete(target, parent);
        } else {
            twoChildrenDelete(target);
        }
    }

    /**
     * Gets the value of an element in the tree that is identified by the
     * provided {@code key} if it exists.
     * <p>
     * If a node with the provided {@code key} is in the tree, the tree is
     * splayed at the found node. If there is no such node, the tree is splayed
     * at the last visited node.
     *
     * @param key The key that identifies the searched value.
     * @return The value associated with the provided {@code key} or
     * {@code null} if there is no such node in the tree.
     */
    @Override
    public V get(K key) {
        LinkedList<Node> searchPath = getSearchPath(key);
        Node result = searchPath.poll();

        // We found a node with the key if the result is not null.
        if (result != null) {
            splay(result);
            return result.value;
        } else {

            // If the path is not empty, we visited a node before we failed.
            if (!searchPath.isEmpty()) {
                Node lastVisit = searchPath.poll();
                splay(lastVisit);
            }

            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<V> get(K min, K max) {
        if (root == null) {

            // We return an empty list if the tree is empty.
            return new ArrayList<>();
        } else {
            List<Node> resultNodes = recursiveSearch(root, min, max);
            List<V> resultValues = new ArrayList<>();

            for (Node n : resultNodes) {
                resultValues.add(n.value);
            }

            return resultValues;
        }
    }

    /**
     * Searches for a node with the provided {@code key} and checks if such a
     * node is in the tree.
     * <p>
     * Because the {@link #get(Comparable)} method gets invoked, the tree is
     * spread as described in {@link #get(Comparable)}.
     *
     * @param key The key that identifies the searched element.
     * @return {@code True} if the searched element is in the tree,
     * {@code false} if not.
     */
    @Override
    public boolean containsKey(K key) {
        return get(key) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Tree: " + root;
    }

    /**
     * Searches recursively for the key of the provided new node {@code newNode}
     * and attaches the new node to the correct parent node.
     *
     * @param subTreeRoot The root of the (sub)tree that is currently being
     *                    searched.
     * @param newNode     The node to be appended.
     */
    private void recursiveAdd(Node subTreeRoot, Node newNode) {
        if (newNode.key.compareTo(subTreeRoot.key) < 0) {
            if (subTreeRoot.leftChild == null) {
                subTreeRoot.leftChild = newNode;

                // Splay at inserted node.
                splay(newNode);
            } else {
                recursiveAdd(subTreeRoot.leftChild, newNode);
            }
        } else if (newNode.key.compareTo(subTreeRoot.key) > 0) {
            if (subTreeRoot.rightChild == null) {
                subTreeRoot.rightChild = newNode;

                // Splay at inserted node.
                splay(newNode);
            } else {
                recursiveAdd(subTreeRoot.rightChild, newNode);
            }
        }
    }

    /**
     * Gets the search path that is traversed when searching for the provided
     * {@code key}.
     *
     * @param key The searched key.
     * @return A {@link LinkedList} containing all visited nodes in visited
     * order.
     */
    private LinkedList<Node> getSearchPath(K key) {
        LinkedList<Node> searchPath = new LinkedList<>();
        Node currentRoot = root;
        boolean endOfPath = false;

        while (!endOfPath) {
            searchPath.push(currentRoot);

            // We are done searching if we find the searched node or null.
            if (currentRoot == null || currentRoot.key.equals(key)) {
                endOfPath = true;
            } else if (key.compareTo(currentRoot.key) < 0) {

                // Continue search in the left subtree.
                currentRoot = currentRoot.leftChild;
            } else {

                // Continue search in the right subtree.
                currentRoot = currentRoot.rightChild;
            }
        }

        return searchPath;
    }

    /**
     * Gets all nodes whose key lie within a provided interval.
     *
     * @param subTreeRoot The root of the (sub)tree whose nodes are currently
     *                    being searched.
     * @param min         The start of the interval.
     * @param max         The end of the interval.
     * @return A {@link List} of all nodes in the (sub)tree that lie within the
     * interval.
     */
    private List<Node> recursiveSearch(Node subTreeRoot, K min, K max) {
        List<Node> result = new ArrayList<>();

        if ((min.compareTo(subTreeRoot.key) < 0)
                && subTreeRoot.leftChild != null) {

            // Add all nodes of the left subtree that lie in the interval.
            result.addAll(recursiveSearch(subTreeRoot.leftChild, min, max));
        }

        if (isInRange(subTreeRoot.key, min, max)) {
            result.add(subTreeRoot);
        }

        if ((max.compareTo(subTreeRoot.key) > 0)
                && subTreeRoot.rightChild != null) {

            // Add all nodes of the right subtree that lie in the interval.
            result.addAll(recursiveSearch(subTreeRoot.rightChild, min, max));
        }

        return result;
    }

    /**
     * Checks if a key {@code value} lies within a provided range.
     *
     * @param value The key value that is examined.
     * @param min   The minimum value of the range.
     * @param max   The maximum value of the range.
     * @return {@code True} if the {@code value} lies within the range
     * {@code false} if not.
     */
    private boolean isInRange(K value, K min, K max) {
        boolean greaterOrEqual = value.compareTo(min) >= 0;
        boolean lessOrEqual = value.compareTo(max) <= 0;

        return greaterOrEqual && lessOrEqual;
    }

    /**
     * Removes a node without children from the tree.
     *
     * @param target The node to be deleted.
     * @param parent The parent node of the node to be deleted.
     */
    private void noChildDelete(Node target, Node parent) {

        // Only the root node has null as parent. (Nodes in the tree)
        if (parent == null) {
            root = null;
        } else if (parent.leftChild == target) {
            parent.leftChild = null;
            splay(parent);
        } else {
            parent.rightChild = null;
            splay(parent);
        }
    }

    /**
     * Removes a node with a left (but no right) child from the tree.
     *
     * @param target The node to be deleted.
     * @param parent The parent node of the node to be deleted.
     */
    private void leftChildDelete(Node target, Node parent) {
        if (parent == null) {
            root = target.leftChild;
        } else if (parent.leftChild == target) {
            parent.leftChild = target.leftChild;
            splay(parent);
        } else {
            parent.rightChild = target.leftChild;
            splay(parent);
        }
    }

    /**
     * Removes a node with a right (but no left) child from the tree.
     *
     * @param target The node to be deleted.
     * @param parent The parent node of the node to be deleted.
     */
    private void rightChildDelete(Node target, Node parent) {
        if (parent == null) {
            root = target.rightChild;
        } else if (parent.leftChild == target) {
            parent.leftChild = target.rightChild;
            splay(parent);
        } else {
            parent.rightChild = target.rightChild;
            splay(parent);
        }
    }

    /**
     * Removes a node with left and right child from the tree.
     *
     * @param target The node to be deleted.
     */
    private void twoChildrenDelete(Node target) {

        // We want the path to the successor of the target.
        LinkedList<Node> pathToMinNode = getPathToMinNode(target.rightChild);
        Node successor = pathToMinNode.poll();
        Node successorParent = pathToMinNode.poll();

        // The list was empty if the successor is the right child of the target.
        if (successorParent == null) {
            successorParent = target;
        }

        target.key = successor.key;
        target.value = successor.value;

        if (successor.leftChild == null && successor.rightChild == null) {
            noChildDelete(successor, successorParent);
        } else {
            rightChildDelete(successor, successorParent);
        }
    }

    /**
     * Gets the search path that is traversed when searching for the node with
     * the smallest key in a (sub)tree with {@code subTreeRoot} as root.
     *
     * @param subTreeRoot The root of the (sub)tree whose minimum is searched
     *                    for.
     * @return A {@link LinkedList} containing all visited nodes in visited
     * order.
     */
    private LinkedList<Node> getPathToMinNode(Node subTreeRoot) {
        if (subTreeRoot == null) {
            throw new IllegalArgumentException("No min-node in empty Tree.");
        }

        LinkedList<Node> path = new LinkedList<>();
        Node currentRoot = subTreeRoot;
        boolean endOfPath = false;

        while (!endOfPath) {
            path.push(currentRoot);

            if (currentRoot.leftChild == null) {
                endOfPath = true;
            } else {

                // Continue search for the minimum in the left subtree.
                currentRoot = currentRoot.leftChild;
            }
        }

        return path;
    }

    /**
     * Rotates the (sub)tree to the right at the provided node.
     *
     * @param subTreeRoot The node to rotate to the right.
     * @param parent      The parent node of the node to rotate.
     */
    private void rightRotate(Node subTreeRoot, Node parent) {
        Node newSubTreeRoot = subTreeRoot.leftChild;

        if (newSubTreeRoot != null) {
            setParentForNewRoot(subTreeRoot, newSubTreeRoot, parent);

            subTreeRoot.leftChild = newSubTreeRoot.rightChild;
            newSubTreeRoot.rightChild = subTreeRoot;
        } else {
            throw new IllegalArgumentException("Cannot rotate that node.");
        }
    }

    /**
     * Rotates the (sub)tree to the left at the provided node.
     *
     * @param subTreeRoot The node to rotate to the left.
     * @param parent      The parent node of the node to rotate.
     */
    private void leftRotate(Node subTreeRoot, Node parent) {
        Node newSubtreeRoot = subTreeRoot.rightChild;

        if (newSubtreeRoot != null) {
            setParentForNewRoot(subTreeRoot, newSubtreeRoot, parent);

            subTreeRoot.rightChild = newSubtreeRoot.leftChild;
            newSubtreeRoot.leftChild = subTreeRoot;
        } else {
            throw new IllegalArgumentException("Cannot rotate that node.");
        }
    }

    /**
     * Sets the parent of the old (sub)tree root to the new subtree root.
     * The parent of the old (sub)tree root now points to the new subtree root
     * instead of the old (sub)tree root.
     *
     * @param oldRoot The old root of the (sub)tree.
     * @param newRoot The new root of the (sub)tree.
     * @param parent  The parent of the old root.
     */
    private void setParentForNewRoot(Node oldRoot, Node newRoot, Node parent) {
        if (parent == null) {
            root = newRoot;
        } else {
            if (oldRoot == parent.leftChild) {
                parent.leftChild = newRoot;
            } else {
                parent.rightChild = newRoot;
            }
        }
    }

    /**
     * Rotates the (sub)tree the correct way, so that the {@code child} becomes
     * the root.
     *
     * @param subTreeRoot The node to rotate.
     * @param child       The node that becomes the new root caused by rotation.
     * @param parent      The parent node of the (sub)tree root.
     */
    private void rotate(Node subTreeRoot, Node child, Node parent) {
        if (child == subTreeRoot.leftChild) {
            rightRotate(subTreeRoot, parent);
        } else {
            leftRotate(subTreeRoot, parent);
        }
    }

    /**
     * Splays the tree at the provided node. This ensures that the provided node
     * becomes the root.
     * <p>
     * For reuse: The passed node must be in the tree, otherwise a
     * {@link NullPointerException} occurs.
     *
     * @param target A node inside the tree.
     */
    private void splay(Node target) {
        LinkedList<Node> path = getSearchPath(target.key);
        path.poll();

        // We keep splaying until the target is the root.
        while (target != root) {
            Node parent = path.poll();
            Node grandParent = path.poll();

            if (grandParent == null) {
                rotate(parent, target, null);
            } else {

                // We cannot poll because we might need this node later.
                Node greatGrandParent = path.peek();

                if ((target == parent.leftChild
                        && parent == grandParent.leftChild)
                        || (target == parent.rightChild
                        && parent == grandParent.rightChild)) {
                    rotate(grandParent, parent, greatGrandParent);
                    rotate(parent, target, greatGrandParent);
                } else {
                    rotate(parent, target, grandParent);
                    rotate(grandParent, target, greatGrandParent);
                }
            }
        }
    }
}
